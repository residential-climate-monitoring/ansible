# Ansible

Ansible scripts to maintain and manage the collection of RCM stations.

# Installation

On Raspberry Pi:

`sudo apt-get install ansible`

On MacOS:

`brew install ansible`


# Usage

To check if all hosts are up:

`ansible --inventory-file hosts.yml -m ping all`
 
To run a playbook:

`ansible-playbook --inventory-file hosts.yml playbooks/ping.yml`